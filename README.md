locBR do Od
====================================
 
Escopo desse repo
-----------------
 
Este projeto contêm os principais módulos da localização brasileira do Odoo, estes módulos são a base dos recursos:
 
* Fiscais
* Contábil
* Sped
 

[//]: # (addons)
Available addons
----------------
addon | version | summary
--- | --- | ---
[l10n_br_account](l10n_br_account/) | 8.0.1.1.0 | Brazilian Localization Account
[l10n_br_account_product](l10n_br_account_product/) | 8.0.2.0.1 | Brazilian Localization Account Product
[l10n_br_account_product_service](l10n_br_account_product_service/) | 8.0.1.0.0 | Brazilian Localization Account Product and Service
[l10n_br_account_service](l10n_br_account_service/) | 8.0.1.0.0 | Brazilian Localization Account Service
[l10n_br_base](l10n_br_base/) | 8.0.1.0.1 | Brazilian Localization Base
[l10n_br_crm](l10n_br_crm/) | 8.0.1.0.1 | Brazilian Localization CRM
[l10n_br_crm_zip](l10n_br_crm_zip/) | 8.0.1.0.1 | Brazilian Localization CRM Zip
[l10n_br_data_account](l10n_br_data_account/) | 8.0.1.0.1 | Brazilian Localisation Data Extension for Account
[l10n_br_data_account_product](l10n_br_data_account_product/) | 8.0.1.0.0 | Brazilian Localisation Data Extension for Product
[l10n_br_data_account_service](l10n_br_data_account_service/) | 8.0.1.0.0 | Brazilian Localization Data Account for Service
[l10n_br_data_base](l10n_br_data_base/) | 8.0.1.0.1 | Brazilian Localisation Data Extension for Base
[l10n_br_delivery](l10n_br_delivery/) | 8.0.1.0.0 | Brazilian Localization Delivery
[l10n_br_purchase](l10n_br_purchase/) | 8.0.1.0.0 | Brazilian Localization Purchase
[l10n_br_sale](l10n_br_sale/) | 8.0.1.0.0 | Brazilian Localization Sale
[l10n_br_sale_product](l10n_br_sale_product/) | 8.0.1.0.0 | Brazilian Localization Sale Product
[l10n_br_sale_service](l10n_br_sale_service/) | 8.0.1.0.0 | Brazilian Localization Sale Service
[l10n_br_sale_stock](l10n_br_sale_stock/) | 8.0.1.0.0 | Brazilian Localization Sales and Warehouse
[l10n_br_stock](l10n_br_stock/) | 8.0.1.0.0 | Brazilian Localization Warehouse
[l10n_br_stock_account](l10n_br_stock_account/) | 8.0.1.0.1 | Brazilian Localization WMS Accounting
[l10n_br_stock_account_report](l10n_br_stock_account_report/) | 8.0.1.0.0 | Brazilian Localization WMS Accounting Report
[l10n_br_zip](l10n_br_zip/) | 8.0.1.0.1 | Brazilian Localisation ZIP Codes
[l10n_br_zip_correios](l10n_br_zip_correios/) | 8.0.1.0.0 | Address from Brazilian Localization ZIP by Correios

Unported addons
---------------
addon | version | summary
--- | --- | ---
[l10n_br_account_payment](l10n_br_account_payment/) | 7.0 (unported) | Brazilian Localization Account Payment
[l10n_br_account_voucher](l10n_br_account_voucher/) | 7.0 (unported) | Brazilian Localization Account Voucher
[l10n_br_hr_timesheet_invoice](l10n_br_hr_timesheet_invoice/) | 1.0 (unported) | Brazilian Invoice on Timesheets

[//]: # (end addons)